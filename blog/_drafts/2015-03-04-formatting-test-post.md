---
layout: post
title: Formatting Test Post
location: Athens, Ohio
comments: true
hide-related: true
---

This is a test post that I am using to help style and format certain type of content for the site. It may change over time, but you can mostly ignore it. Post a comment if you see anything that looks wrong (or could look better).

## Ad quorum et cognitionem et usum iam corroborati natura ipsa praeeunte deducimur.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quae cum dixisset, finem ille. Idemque diviserunt naturam hominis in animum et corpus. Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt; `Paria sunt igitur.` Duo Reges: constructio interrete. Ut optime, secundum naturam affectum esse possit.

* Ita redarguitur ipse a sese, convincunturque scripta eius probitate ipsius ac moribus.
* Quo plebiscito decreta a senatu est consuli quaestio Cn.
* Polycratem Samium felicem appellabant.
* Illud dico, ea, quae dicat, praeclare inter se cohaerere.
* Scaevolam M.

### Bestiarum vero nullum iudicium puto.

**Stoicos roga.** In eo enim positum est id, quod dicimus esse expetendum. Estne, quaeso, inquam, sitienti in bibendo voluptas? Haec quo modo conveniant, non sane intellego. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Cur igitur, cum de re conveniat, non malumus usitate loqui? Quorum sine causa fieri nihil putandum est. [Peccata paria](#). Ut id aliis narrare gestiant? An nisi populari fama? Si autem id non concedatur, non continuo vita beata tollitur. Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. Ex quo illud efficitur, qui bene cenent omnis libenter cenare, qui libenter, non continuo bene.

#### An ea, quae per vinitorem antea consequebatur, per se ipsa curabit?

{% highlight javascript linenos=table %}
var s = "JavaScript syntax highlighting";
alert(s);
{% endhighlight %}

##### Quoniam, si dis placet, ab Epicuro loqui discimus.

[Quod equidem non reprehendo;](#) Quid ergo? **Si longus, levis. Paria sunt igitur.** Ergo illi intellegunt quid Epicurus dicat, ego non intellego? `Audeo dicere, inquit.` Quoniam, si dis placet, ab Epicuro loqui discimus. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Duo enim genera quae erant, fecit tria. Pauca mutat vel plura sane; Sed quid sentiat, non videtis.

{% highlight sass linenos=table %}
@mixin cb-opacity($alpha_val) {
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=#{$alpha_val * 100})";
    filter: alpha(opacity=#{$alpha_val * 100});
    -moz-opacity: $alpha_val;
    -khtml-opacity: $alpha_val;
    opacity: $alpha_val;
}
{% endhighlight %}

###### Pauca mutat vel plura sane

1. Quis Pullum Numitorium Fregellanum, proditorem, quamquam rei publicae nostrae profuit, non odit?
2. Sed id ne cogitari quidem potest quale sit, ut non repugnet ipsum sibi.
3. Qua igitur re ab deo vincitur, si aeternitate non vincitur?
4. Nunc reliqua videamus, nisi aut ad haec, Cato, dicere aliquid vis aut nos iam longiores sumus.
5. Obscura, inquit, quaedam esse confiteor, nec tamen ab illis ita dicuntur de industria, sed inest in rebus ipsis obscuritas.
6. Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri.
