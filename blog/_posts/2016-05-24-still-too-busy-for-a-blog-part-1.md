---
layout: "post"
title: "Still Too Busy for a Blog, Part 1"
location: "Athens, Ohio"
date: 2016-05-24 9am
comments: true
tags: [meta, life]
---

In what is clearly becoming a recurring theme for this site and blog, I've been extremely busy both with work and with life, so the updates will have to come as a large chunk yet again!

## Fall

### September

In September I finally got back out onto the soccer field after many years away, captaining a team named Sherwood Forest in Athens Community Soccer. We also started regularly going to [Little Fish Brewing](https://www.facebook.com/littlefishbrewing){: target="_blank" rel="noreferrer"} which is over past White's Mill on the far west side of Athens.

![Little Fish Brewing new beer release]({{site.url}}/uploads/2016-05/littlefish-release.jpg "Little Fish Brewing new beer release."){: .fullwidth}

In late September I had the fabulous pleasure of seeing [The Wood Brothers](http://www.thewoodbros.com/){: target="_blank" rel="noreferrer"} at Nelsonville's very own Stuart's Opera House. It was my very first time stepping foot in the venue, and I've already been back a few times since!

![The Wood Brothers at Stuart's Opera House on September 28th, 2015]({{site.url}}/uploads/2016-05/woodbros-stuarts.jpg "The Wood Brothers at Stuart's Opera House on September 28th, 2015"){: .fullwidth}

### October

October in Athens is summed up by two very, very big events: Homecoming and Halloween. Halloween may be bigger, and more well known, but it is Homecoming that is truly the best day in Athens of the entire year.

[![Homecoming Parade, 2015]({{site.url}}/uploads/2016-05/homecoming-2015.jpg "Homecoming Parade, 2015")](https://www.ohio.edu/compass/stories/15-16/10/Homecoming-2015-Highlights.cfm){: target="_blank" rel="noreferrer" .image-link .fullwidth}

In mid-October, the stars **finally** aligned, and I was able to see Modest Mouse. It was in Columbus outdoors at the LC (now EXPRESS LIVE) and was a magical, amazing night.

![Modest Mouse at the LC on October 16th, 2015]({{site.url}}/uploads/2016-05/mm-columbus.jpg "Modest Mouse at the LC on October 16th, 2015"){: .fullwidth}

### November

November was fairly uneventful compared to a lot of others, but we still had our first, proper Thanksgiving Day feast in the new apartment, which was great.

[![The fantastic meal.]({{site.url}}/uploads/2016-05/me-tday_thumb.jpg "The fantastic meal.")]({{site.url}}/uploads/2016-05/me-tday_full.jpg){: target="_blank" rel="noreferrer" .image-link .fullwidth}

## Winter

### December

Very early in December, I was lucky enough to be able to go to both Sleater-Kinney at the Newport _and_ the MLS Cup Final in Columbus between the Crew and the Portland Timbers. Both nights lived up to their hype, even though the Crew couldn't quite pull out a win.

![Sleater-Kinney at the Newport on December 5th, 2015]({{site.url}}/uploads/2016-05/sleaterkinney-columbus.jpg "Sleater-Kinney at the Newport on December 5th, 2015"){: .fullwidth}

![Tailgating for the 2015 MLS Cup Final in Columbus.]({{site.url}}/uploads/2016-05/crew-mls-final.jpg "Tailgating for the 2015 MLS Cup Final in Columbus."){: .fullwidth}

And a very "special" mention to Greyhound... who royally screwed up my New Year's travel plans to Denver. 6 months later a refund on the tickets was finally received, and Greyhound is no longer a travel option in my brain whatsoever. I cannot recommend staying away from them enough. So here's Heather and I as we "slept" in the terminal in Chicago overnight waiting to get back to Columbus, never making it any further west.

[![A panorama of a place I never want to visit again.]({{site.url}}/uploads/2016-05/chicago-greyhound_thumb.jpg "A panorama of a place I never want to visit again.")]({{site.url}}/uploads/2016-05/chicago-greyhound_full.jpg){: target="_blank" rel="noreferrer" .image-link .fullwidth}

### January

January was, unsurprisingly, a pretty cold and snowy month here in Athens. This led to some interesting trips and creations...

![Our front-yard snowman with his whiskey hat and stick-guitar.]({{site.url}}/uploads/2016-05/yard-snowman.jpg "Our front-yard snowman with his whiskey hat and stick-guitar."){: .fullwidth}

![It was actually about 60 degrees when these were taken, it just really didn't look like it!]({{site.url}}/uploads/2016-05/heather-waterfall.jpg "It was actually about 60 degrees when these were taken, it just really didn't look like it!"){: .fullwidth}

### February

February brought about more hiking! We've got the gear and the areas around us to take amazing day-trips out into the wilderness.

![Heather hiking a ridge in the Hocking Hills.]({{site.url}}/uploads/2016-05/heather-ridgeline.jpg "Heather hiking a ridge in the Hocking Hills."){: .fullwidth}

Later in the month, we went to the Sock-Hop at Rollerbowl Lanes which was definitely a new experience.

[![A crop from a Polaroid of our 'costumes' for the sock-hop.]({{site.url}}/uploads/2016-05/sock-hop.jpg "A crop from a Polaroid of our 'costumes' for the sock-hop.")](https://www.youtube.com/watch?v=TCU_ipJgYEM){: target="_blank rel="noreferrer"" .image-link .fullwidth}

February turned out to be pretty great as I also caught Animal Collective at the Newport in Columbus.

[![Animal Collective at the Newport on February 26th, 2016]({{site.url}}/uploads/2016-05/animalcollective-columbus.jpg "Animal Collective at the Newport on February 26th, 2016")](https://www.youtube.com/watch?v=RETdcEYbgX4){: target="_blank" rel="noreferrer" .image-link .fullwidth}

On extra-special-bonus-day, or Leap Day, it was back to Columbus yet again for the show that Heather got tickets to for me for Christmas, one of my favorite bands in the world, Coheed and Cambria.

![Claudio Sanchez of Coheed and Cambria playing at EXPRESS LIVE on February 29th, 2016.]({{site.url}}/uploads/2016-05/coheed-columbus.jpg "Claudio Sanchez of Coheed and Cambria playing at EXPRESS LIVE on February 29th, 2016."){: .fullwidth}

## Continued in [Part 2]({{site.url}}/blog/still-too-busy-for-a-blog-part-2/)
