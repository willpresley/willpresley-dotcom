---
layout: "post"
title: "Presley - Withee Wedding | June 22nd, 2018"
location: "Athens, Ohio"
date: 2018-06-23 9am
comments: true
tags: ["meta", "life"]
---

## A Note from *the Future*

Not really, but I have been genuinely awful about keeping up this blog, but a plan of mine for 2020 is to fix that! I felt really weird just jump-starting after nearly 5 years into new stuff, so there will be a few yearly recaps (that are mainly comprised of images) first! They will get rational dates instead of all being January 2020.

## The Big Reveal

Heather and I got married on our 9 year anniversary, at our fantastic neighbors the West End Cider House, but didn't tell anyone until December 31st, 2018. Our friend Nicholas Cappanna performing a perfect little pagan ceremony with just a handful of friends.

## The Photos

Click/tap on a photo to view it full-size.

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_001_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_001_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_002_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_002_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_003_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_003_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_004_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_004_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_005_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_005_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_006_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_006_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_007_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_007_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_008_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_008_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_009_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_009_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_010_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_010_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_011_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_011_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_012_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_012_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_013_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_013_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_014_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_014_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_015_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_015_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_016_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_016_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_017_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_017_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_018_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_018_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_019_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_019_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_020_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_020_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_021_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_021_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Presley-Withee Wedding]({{site.url}}/uploads/2018-06/thumbs/presley-withee-wedding_022_blog_thumb.jpg "Presley-Withee Wedding")]({{site.url}}/uploads/2018-06/presley-withee-wedding_022_blog.jpg){: target="_blank" .image-link .fullwidth}

</div>
