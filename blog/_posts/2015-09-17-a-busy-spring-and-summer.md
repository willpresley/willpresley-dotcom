---
layout: "post"
title: "A Busy Spring and Summer"
location: "Athens, Ohio"
date: 2015-09-17 9am
comments: true
tags: [meta, life]
---

As can be clearly seen by the lack of updates on this site, the last 6 months have been rather hectic for me both personally and professionally. Here's a (very) brief overview of how things have been going!

## Spring

### March

In March there was a tremendous snow storm here in Athens, and a couple days later we got [these pictures](https://www.facebook.com/electronic.vision.ev/posts/925654360812603){: target="_blank" rel="noreferrer"} of our gorgeous town from the company quadcopter:

[![March snowstorm in Athens]({{site.url}}/uploads/2015-09/march-snowstorm.jpg "March snowstorm in Athens")](https://www.facebook.com/electronic.vision.ev/posts/925654360812603){: target="_blank" rel="noreferrer" .image-link .fullwidth}

### April

In April I got the chance to see the Columbus Alternative Fashion Week (AFW) Grand Finale show at the Idea Foundry. The show was fantastic and the designer we were supporting did a great job.

[![AFW Finale show]({{site.url}}/uploads/2015-09/afw-static.jpg "AFW Finale show")](http://i.imgur.com/yUbboe0.gifv){: target="_blank" rel="noreferrer" .image-link .fullwidth}

### May

In May we moved into the bigger and nicer apartment next door to where we had lived for 3 years. It gets more sun, has much more space, and will be a perfect platform to move forward!

At the end of the month we finally _properly_ attended the [Nelsonville Music Festival](http://www.nelsonvillefest.org/){: target="_blank" rel="noreferrer"}, headlined in 2015 by The Flaming Lips, St. Vincent, Trampled by Turtles, and Merle Haggard. The festival is an absolute blast, and I highly recommend it.

![The Flaming Lips at NMF 2015]({{site.url}}/uploads/2015-09/nmf-flaming-lips.jpg "The Flaming Lips at NMF 2015"){: .fullwidth}

## Summer

### June

June means one thing, and one thing only.. **BONNAROO**

Every single set was great, my job helping run the [festival Lost & Found](http://www.mtv.com/news/2186375/bonnaroo-lost-and-found-2015/){: target="_blank" rel="noreferrer"} went well, and I had an amazing time at my sixth Bonnaroo.

![My Morning Jacket at Bonnaroo 2015]({{site.url}}/uploads/2015-09/roo-mmj-2015.jpg "My Morning Jacket at Bonnaroo 2015"){: .fullwidth}

### July

July for me was just full of work, rain, and amazing scenes like this, a double-rainbow over the [West End Ciderhouse](http://www.westendciderhouse.com/){: target="_blank" rel="noreferrer"}.

![West End Ciderhouse Double Rainbow]({{site.url}}/uploads/2015-09/ciderhouse-double-rainbow.jpg "West End Ciderhouse Double Rainbow"){: .fullwidth}

### August

In August I still had a bunch to get done at work, and also decided to get back into playing soccer on the weekends. No great pictures, but here's me in my office at EV!

![Me in the office at EV]({{site.url}}/uploads/2015-09/me-in-the-office.jpg "Me in the office at EV"){: .fullwidth}

### September and beyond

Now the plan is to get back on track with some freelance and hobby projects, and get this website tuned and filled with content properly. Things have come to a steady level at EV, my soccer league has started, and another amazing fall in Athens is getting underway!
