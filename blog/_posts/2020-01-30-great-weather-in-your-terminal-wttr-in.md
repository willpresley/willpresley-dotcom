---
layout: "post"
title: "Great Weather in Your Terminal: wttr.in"
location: "Athens, Ohio"
date: 2020-01-30 9am
comments: true
tags: ["terminal", "shortposts", "shares"]
excerpt: "Test excerpt"
---

As has been the case with a lot of things lately, I spotted this first on [Jan-Lukas Else's blog](https://jlelse.blog/links/2020/01/wttr-in/) (who spotted it [on Henrique Dias' blog](https://hacdias.com/2020/01/20/4/weather-terminal/)). It's a great (and heavily-starred) project called [wttr.in](https://wttr.in) that brings weather forecasts into your terminal with a huge amount of options. The amount of [supported output formats](https://github.com/chubin/wttr.in#supported-output-formats) is very impressive. Check out the [GitHub repository](https://github.com/chubin/wttr.in) and here is an example that I have aliased to 'weather' in my WSL2 bash setup:

`curl wttr.in/~Ohio+University+Athens+Ohio`

[![Current Weather in Athens, Ohio as seen in the terminal using wttr.in]({{site.url}}/uploads/2020-01/wttr-example.png "Current Weather in Athens, Ohio as seen in the terminal using wttr.in")]({{site.url}}/uploads/2020-01/wttr-example.png){: target="_blank" .image-link .fullwidth}
