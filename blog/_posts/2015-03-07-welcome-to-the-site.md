---
layout: "post"
title: "Welcome to WillPresley.com!"
location: "Athens, Ohio"
date: 2015-03-07 9am
comments: true
tags: [meta]
---

Welcome to the new and improved WillPresley.com!

![Me in the office at EV in 2015]({{site.url}}/uploads/2015-09/me-in-the-office.jpg "Me in the office at EV in 2015"){: .fullwidth}

I think that my name is plastered on the site enough, but I'm Will. I'm currently {{'now' | date: "%s" | minus : 620366400 | divided_by: 31536000}} years old and live in {{ site.location }}. I have long been obsessed with computers and technology, and have been making websites since I was 11 years old.

### How I Got Into Web Development

My first website that was seen by the public was a fan site and download center for the PC game series [RollerCoaster Tycoon](http://en.wikipedia.org/wiki/RollerCoaster_Tycoon). Called **RCT Market** and launched originally on GeoCities sometime around 2001, it first appears in the Archive.org Wayback Machine [in April of 2002](http://web.archive.org/web/20020426014315/http://rctmarket.rctoa.com/) (v2, a custom PHP incarnation). The experience of building and managing both a site and a community was a fantastic opportunity for a 12-13 year old, and helped set me along towards my future career choice.

After RCT Market, I focused mostly on school and playing soccer for quite a few years. One semi-successful website I managed during this time was a general purpose gaming hub called GamingChamber.com. Gaming Chamber was, at its peak, a community of around 200 users who discussed console and PC gaming, and also regularly joined up to play with/against each other in online games. One of the final initiatives of the site staff before the site folded was a short-lived, two-issue "magazine" for the handheld Sony PSP called I^2 (Industry Insider), one of the first (and in retrospect, last) of its kind.

During my last year of high school in Columbus, Ohio and into my first year of college at Ohio University in Athens, I reached out to a couple of guys from England who were already working on the PC Gaming-focused website _De-Frag.com_. They seemed like they had a lot of great content, but could use a site administrator/developer, so I offered them my services and we worked together for close to two years. I also contributed to the writing team for both news and reviews, a similar 'Developer/Writer' role to those I performed on both RCT Market and Gaming Chamber.

### What I Do for a Living Now

In 2013 I became a professional web developer for **Electronic Vision**, a multimedia company in Athens with a long and rich history of being the most cutting-edge shop in the area. A large portion of my projects for EV were full web rebuilds for clients in one of a number of platforms including Wordpress, CommonSpot, Moodle, MantisBT, Drupal, and more. In 2016, Electronic Vision became [Red Tail Design Company](https://redtaildesign.com), which I had the fortune of helping start up and continue to help run today. We have refined our craft while expanding into woodworking, laser etching/cutting, and product development for sister company [Ohio is Home](https://ohioishome.com).

My **home** development machine is running Windows 10 machine with an i5-4590, EVGA nVidia GeForce 1050 Ti SC, 16GB of RAM, and a dual monitor setup. I use Windows Subsystem for Linux (WSL) running Ubuntu 18.04 from the command line inside of [ConEmu](https://github.com/Maximus5/ConEmu) as my terminal, and also use VMWare Workstation and VirtualBox to emulate a large number of other platforms when needed.

My **work** development machine is also Windows 10, with an i5-4460, ASUS nVidia GeForce 640 GT, 16GB of RAM, and a triple monitor setup. I use Cygwin to emulate or replace a lot of *nix functions, but am slowly migrating over to using WSL at work as well. I keep it familiar with ConEmu as my terminal, and still use VMWare Workstation and VirtualBox to emulate other platforms when needed.

## About This Site

I plan on using this site to share all sorts of things: technical posts/articles, photos, personal blogs, my favorite projects I've worked on, and more. I plan on keeping things simple, and you won't really ever see this site get too flashy. See one of my other blog entries for more info on how I build and deploy this blog, and click on the image in the footer to learn more about _Jekyll_, the static-site building software that generates this website.
