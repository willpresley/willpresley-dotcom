---
layout: "post"
title: "Blog Recap - The Rest of 2016"
location: "Athens, Ohio"
date: 2016-12-31 9am
comments: true
tags: ["meta", "life"]
---

## A Note from *the Future*

Not really, but I have been genuinely awful about keeping up this blog, but a plan of mine for 2020 is to fix that! I felt really weird just jump-starting after nearly 5 years into new stuff, so there will be a few yearly recaps (that are mainly comprised of images) first! They will get rational dates instead of all being January 2020.

{:.text-center .hide-from-rss}
**[June](#june) \| [July](#july) \| [August](#august) \| [September](#september) \| [October](#october) \| [November](#november) \| [December](#december)**

## June

### Nelsonville Music Festival 2016

[![Water Witches at NMF 2016]({{site.url}}/uploads/2016-12/nmf-2016.jpg "Water Witches at NMF 2016")]({{site.url}}/uploads/2016-12/nmf-2016.jpg){: target="_blank" .image-link .fullwidth}

Water Witches at NMF 2016

### Bonnaroo 2016

[![Bonnaroo 2016]({{site.url}}/uploads/2016-12/roo-2016.jpg "Bonnaroo 2016")]({{site.url}}/uploads/2016-12/roo-2016.jpg){: target="_blank" .image-link .fullwidth}

[![Bonnaroo 2016]({{site.url}}/uploads/2016-12/roo-2016-2.jpg "Bonnaroo 2016")]({{site.url}}/uploads/2016-12/roo-2016-2.jpg){: target="_blank" .image-link .fullwidth}

## July

### Ohio Blues in Ann Arbor

[![Ohio Blues in Ann Arbor]({{site.url}}/uploads/2016-12/cfc-in-ann-arbor.jpg "Ohio Blues in Ann Arbor")]({{site.url}}/uploads/2016-12/cfc-in-ann-arbor.jpg){: target="_blank" .image-link .fullwidth}

## August

### Brunch with Kristen and Heather

[![Brunch with Kristen and Heather]({{site.url}}/uploads/2016-12/brunch-with-kristen-and-heather.jpg "Brunch with Kristen and Heather")]({{site.url}}/uploads/2016-12/brunch-with-kristen-and-heather.jpg){: target="_blank" .image-link .fullwidth}

## September

### My Favorite Tree in Athens

[![My Favorite Tree in Athens]({{site.url}}/uploads/2016-12/athens-tree-shot.jpg "My Favorite Tree in Athens")]({{site.url}}/uploads/2016-12/athens-tree-shot.jpg){: target="_blank" .image-link .fullwidth}

## October

### Camping in the Woods of Virginia

[![Camping in the Woods of Virginia]({{site.url}}/uploads/2016-12/virginia-camping.jpg "Camping in the Woods of Virginia")]({{site.url}}/uploads/2016-12/virginia-camping.jpg){: target="_blank" .image-link .fullwidth}

### Having a "Double Office" at Home

[![Having a "Double Office" at Home]({{site.url}}/uploads/2016-12/double-office.jpg "Having a "Double Office" at Home")]({{site.url}}/uploads/2016-12/double-office.jpg){: target="_blank" .image-link .fullwidth}

While we were getting the physical location for Red Tail Design Company and Ohio is Home ready, I brought all of my work desk gear home and had a pretty ridiculous home office setup.

### Ohio University Homecoming 2016

[![Ohio University Homecoming 2016]({{site.url}}/uploads/2016-12/ou-homecoming-2016.jpg "Ohio University Homecoming 2016")]({{site.url}}/uploads/2016-12/ou-homecoming-2016.jpg){: target="_blank" .image-link .fullwidth}

## November

### Putting the Final Touches on RTD and OIH

[![Putting the Final Touches on RTD and OIH]({{site.url}}/uploads/2016-12/store-and-office.jpg "Putting the Final Touches on RTD and OIH")]({{site.url}}/uploads/2016-12/store-and-office.jpg){: target="_blank" .image-link .fullwidth}

[![Putting the Final Touches on RTD and OIH]({{site.url}}/uploads/2016-12/store-and-office-2.jpg "Putting the Final Touches on RTD and OIH")]({{site.url}}/uploads/2016-12/store-and-office-2.jpg){: target="_blank" .image-link .fullwidth}

[![Putting the Final Touches on RTD and OIH]({{site.url}}/uploads/2016-12/store-and-office-3.jpg "Putting the Final Touches on RTD and OIH")]({{site.url}}/uploads/2016-12/store-and-office-3.jpg){: target="_blank" .image-link .fullwidth}

## December

### Soft Opening for Ohio is Home

[![Soft Opening for Ohio is Home]({{site.url}}/uploads/2016-12/store-soft-open.jpg "Soft Opening for Ohio is Home")]({{site.url}}/uploads/2016-12/store-soft-open.jpg){: target="_blank" .image-link .fullwidth}

### Midnight Moon

[![Midnight Moon]({{site.url}}/uploads/2016-12/midnight-moon.jpg "Midnight Moon")]({{site.url}}/uploads/2016-12/midnight-moon.jpg){: target="_blank" .image-link .fullwidth}
