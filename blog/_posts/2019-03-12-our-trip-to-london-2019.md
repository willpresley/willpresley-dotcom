---
layout: "post"
title: "Our Trip to London - 2019"
location: "London, England"
date: 2019-03-12 9am
comments: true
tags: ["meta", "life"]
---

## A Note from *the Future*

Not really, but I have been genuinely awful about keeping up this blog, but a plan of mine for 2020 is to fix that! I felt really weird just jump-starting after nearly 5 years into new stuff, so there will be a few yearly recaps (that are mainly comprised of images) first! They will get rational dates instead of all being January 2020.

## The Setup

Heather and I *finally* traveled outside of the country, and went to London for 10 days!

## The Photos

Click/tap on a photo to view it full-size.

### Sight Seeing

<div markdown="1" class="photo-half remove-top-margin with-caption">

[![Heather and I at Octoberfest Pub]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_001_thumb.jpg "Heather and I at Octoberfest Pub")]({{site.url}}/uploads/2019-03/london-trip-2019_001.jpg){: target="_blank" .image-link .fullwidth}

The first pub we stopped to wasn't even English, it was Octoberfest on Fulham Road.

</div>

<div markdown="1" class="photo-half remove-top-margin with-caption">

[![Piccadilly Circus]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_002_thumb.jpg "Piccadilly Circus")]({{site.url}}/uploads/2019-03/london-trip-2019_002.jpg){: target="_blank" .image-link .fullwidth}

Piccadilly Circus lived up to its reputation, and was easily the most packed place we were the entire trip.

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![British Natural History Museum]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_003_thumb.jpg "British Natural History Museum")]({{site.url}}/uploads/2019-03/london-trip-2019_003.jpg){: target="_blank" .image-link .fullwidth}

[British Natural History Museum](https://www.nhm.ac.uk/)

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![British Natural History Museum]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_004_thumb.jpg "British Natural History Museum")]({{site.url}}/uploads/2019-03/london-trip-2019_004.jpg){: target="_blank" .image-link .fullwidth}

[British Natural History Museum](https://www.nhm.ac.uk/)

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Westminster Abbey]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_005_thumb.jpg "Westminster Abbey")]({{site.url}}/uploads/2019-03/london-trip-2019_005.jpg){: target="_blank" .image-link .fullwidth}

[Westminster Abbey](https://en.wikipedia.org/wiki/Westminster_Abbey) 🤫

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Westminster Abbey]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_006_thumb.jpg "Westminster Abbey")]({{site.url}}/uploads/2019-03/london-trip-2019_006.jpg){: target="_blank" .image-link .fullwidth}

[Westminster Abbey](https://en.wikipedia.org/wiki/Westminster_Abbey) 🤫

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Horse Guards Parade]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_007_thumb.jpg "Horse Guards Parade")]({{site.url}}/uploads/2019-03/london-trip-2019_007.jpg){: target="_blank" .image-link .fullwidth}

[Horse Guards Parade](https://en.wikipedia.org/wiki/Horse_Guards_Parade)

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Monument to Queen Victoria next to Buckingham Palace]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_008_thumb.jpg "Monument to Queen Victoria next to Buckingham Palace")]({{site.url}}/uploads/2019-03/london-trip-2019_008.jpg){: target="_blank" .image-link .fullwidth}

[Victoria Memorial](https://en.wikipedia.org/wiki/Victoria_Memorial,_London), next to Buckingham Palace

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![The basement area of The Chelsea Pensioner]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_009_thumb.jpg "The basement area of The Chelsea Pensioner")]({{site.url}}/uploads/2019-03/london-trip-2019_009.jpg){: target="_blank" .image-link .fullwidth}

The basement area of [The Chelsea Pensioner](https://www.thechelseapensioner.co.uk/)

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Tower of London]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_010_thumb.jpg "Tower of London")]({{site.url}}/uploads/2019-03/london-trip-2019_010.jpg){: target="_blank" .image-link .fullwidth}

[Tower of London](https://en.wikipedia.org/wiki/Tower_of_London)

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Tower of London]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_011_thumb.jpg "Tower of London")]({{site.url}}/uploads/2019-03/london-trip-2019_011.jpg){: target="_blank" .image-link .fullwidth}

[Tower of London](https://en.wikipedia.org/wiki/Tower_of_London)

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Tower of London and Tower Bridge]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_012_thumb.jpg "Tower of London and Tower Bridge")]({{site.url}}/uploads/2019-03/london-trip-2019_012.jpg){: target="_blank" .image-link .fullwidth}

[Tower of London](https://en.wikipedia.org/wiki/Tower_of_London) and [Tower Bridge](https://en.wikipedia.org/wiki/Tower_Bridge)

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Tower of London, Tower Bridge, and me]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_013_thumb.jpg "Tower of London, Tower Bridge, and me")]({{site.url}}/uploads/2019-03/london-trip-2019_013.jpg){: target="_blank" .image-link .fullwidth}

[Tower of London](https://en.wikipedia.org/wiki/Tower_of_London), [Tower Bridge](https://en.wikipedia.org/wiki/Tower_Bridge), and me

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Tower Bridge]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_014_thumb.jpg "Tower Bridge")]({{site.url}}/uploads/2019-03/london-trip-2019_014.jpg){: target="_blank" .image-link .fullwidth}

[Tower Bridge](https://en.wikipedia.org/wiki/Tower_Bridge)

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![The British Museum]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_015_thumb.jpg "The British Museum")]({{site.url}}/uploads/2019-03/london-trip-2019_015.jpg){: target="_blank" .image-link .fullwidth}

[The British Museum](https://www.britishmuseum.org/)

</div>

<div markdown="1" class="photo-half remove-margin with-caption">

[![Tosi Gorgonzola Bar]({{site.url}}/uploads/2019-03/thumbs/london-trip-2019_016_thumb.jpg "Tosi Gorgonzola Bar")]({{site.url}}/uploads/2019-03/london-trip-2019_016.jpg){: target="_blank" .image-link .fullwidth}

Our wonderful host Fabrizio runs an award-winning cheese bar in London called [Tosi](https://www.tosigorgonzolabar.com/) and invited us there for an absolutely amazing night. If you're **ever** in London, you owe it to yourself to check it out!

</div>

### Chelsea FC & Stamford Bridge

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_001_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_001.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_002_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_002.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_003_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_003.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_004_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_004.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_005_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_005.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_006_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_006.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_007_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_007.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_008_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_008.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_009_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_009.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_010_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_010.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_011_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_011.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_012_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_012.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_013_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_013.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_016_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_016.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_017_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_017.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![London Trip 2019]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_018_thumb.jpg "London Trip 2019")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_018.jpg){: target="_blank" .image-link .fullwidth}

</div>

<div markdown="1" class="photo-half remove-margin">

[![Meeting Ron "Chopper" Harris]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_014_thumb.jpg "Meeting Ron "Chopper" Harris")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_014.jpg){: target="_blank" .image-link .fullwidth}

I had the honor of meeting [Ron "Chopper" Harris](https://en.wikipedia.org/wiki/Ron_Harris_(footballer)) with some of the Ohio Blues!

</div>

<div markdown="1" class="photo-half remove-margin">

[![Meeting Bobby Tambling]({{site.url}}/uploads/2019-03/chelsea/thumbs/london-trip-2019-chelsea_015_thumb.jpg "Meeting Bobby Tambling")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_015.jpg){: target="_blank" .image-link .fullwidth}

And also the absolute legend, former Chelsea all-time leading goalscorer (now #2 to Frank Lampard) [Mr. Bobby Tambling](https://en.wikipedia.org/wiki/Bobby_Tambling)!

</div>

[![Meeting Paul Canoville and Pat Nevin!]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_019.jpg "Meeting Paul Canoville and Pat Nevin!")]({{site.url}}/uploads/2019-03/chelsea/london-trip-2019-chelsea_019.jpg){: target="_blank" .image-link .fullwidth}

Meeting [Pat Nevin](https://en.wikipedia.org/wiki/Pat_Nevin) and [Paul Canoville](https://en.wikipedia.org/wiki/Paul_Canoville) as well!
