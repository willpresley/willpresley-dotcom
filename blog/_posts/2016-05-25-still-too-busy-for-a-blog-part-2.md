---
layout: "post"
title: "Still Too Busy for a Blog, Part 2"
location: "Athens, Ohio"
date: 2016-05-25 9am
comments: true
tags: ["meta", "life"]
---

Continued from [Part 1]({{site.url}}/blog/still-too-busy-for-a-blog-part-1/)..

## Spring

### March

Springtime in Athens is always fantastic, and this one was no exception. After some much-needed fort building and hermit-holing at the apartment, the creative juices started flowing again.

[![The wall outside of our apartment facing the road.]({{site.url}}/uploads/2016-05/westsidebestside-wall.jpg "The wall outside of our apartment facing the road.")](https://www.instagram.com/p/BDedYnuke2Q/){: target="_blank" .image-link rel="noreferrer" .fullwidth}

The run of amazing concerts was further extended by catching Dr. Dog at the Newport. The show was honestly incredible, and they are worth seeing over and over.

![Dr. Dog playing at the Newport on March 29th, 2016.]({{site.url}}/uploads/2016-05/drdog-columbus.jpg "Dr. Dog playing at the Newport on March 29th, 2016."){: .fullwidth}

### April

In April work _finally_ started on the garden/flowerbed outside of the new apartment. I will have actual pictures of things growing soon, but needless to say, it's been a success.

[![One of my favorite beers along with an opener from ohioishome.com]({{site.url}}/uploads/2016-05/garden-beer.jpg "One of my favorite beers along with an opener from ohioishome.com")](https://ohioishome.com){: target="_blank" .image-link rel="noreferrer" .fullwidth}

We also returned to Louisville, Kentucky for Thunder Over Louisville, where lots of carnival-type things were done and I took pictures for an upcoming project.

![Fireworks over the midway in Louisville.]({{site.url}}/uploads/2016-05/thunder-fireworks.jpg "Fireworks over the midway in Louisville."){: .fullwidth}

And lastly for April was one of the better Sunday Funday events at the [Jackie O's](http://jackieos.com/){: target="_blank" rel="noreferrer"} Taproom, a beach party featuring some of our favorite local bands like Water Witches and The D-Rays!

![Sunday Funday at Jackie O's featuring The D-Rays.]({{site.url}}/uploads/2016-05/sunday-funday.jpg "Sunday Funday at Jackie O's featuring The D-Rays."){: .fullwidth}

### May

May has been, thankfully, quite a bit more relaxed than a lot of months. This is just the calm before the storm, however, as Nelsonville Music Festival and Bonnaroo are both _just_ around the corner! One thing that **has** come to Athens in huge numbers, for the first time in 17 years, is the cicada. There are thousands and thousands of [confirmed reports](http://www.magicicada.org/databases/magicicada/map.html){: target="_blank" rel="noreferrer"} in our area, and there are at least a few on our property already as well.

![A cicada hanging out in the bush by our front porch.]({{site.url}}/uploads/2016-05/bush-cicada.jpg "A cicada hanging out in the bush by our front porch."){: .fullwidth}

### June and Beyond!

As usual, I will in this paragraph make grand promises about how I will "update this site more" and so on. However, that is really my plan, and some recent happenings in my professional life will mean that I will soon have many more projects to put into the portfolio here. So check back soon for more!
