---
layout:       project
published:    false
date:         20150101
short:        "(short description)"
title:        "(title)"
website:      http://www.willpresley.com
image-small:
  url:        "https://willpresley.com/uploads/projects/sample/sample-small_1.png"
  title:      "(title)"
  alt:        "(title)"
image-large:
  url:        "https://willpresley.com/uploads/projects/sample/sample-large_1.png"
  title:      "(title)"
  alt:        "(title)"
  bordered:   false
---
_Project for ________._

(long description)

## List title

* (list items on things done)
