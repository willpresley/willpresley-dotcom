---
layout:       project
published:    true
date:         20150814
short:        "Custom responsive Wordpress theme utilizing the Customizer API as well as custom plugins."
title:        "Integrated Services for Behavioral Health"
image-small:
  url:        "https://willpresley.com/uploads/projects/integratedservice/integratedservice-small_1.jpg"
  title:      "Integrated Services for Behavioral Health"
  alt:        "Integrated Services for Behavioral Health"
image-large:
  url:        "https://willpresley.com/uploads/projects/integratedservice/integratedservice-large_1.jpg"
  title:      "Integrated Services for Behavioral Health"
  alt:        "Integrated Services for Behavioral Health"
  bordered:   true
---
_Project for Electronic Vision._

Integrated Services came to EV looking to go through a minor re-brand while they expanded operations throughout the Midwest. Part of this overhaul was a full redesign and rebuild of their website. Wordpress was chosen due to many project factors and a fully custom theme was developed by me to tackle the many challenges of the build. The result is a clean, fast, responsive site with limitless potential for expansion.

## Some of what this project entailed..

* An entirely custom responsive Wordpress theme based on a design mockup by the EV creative director.
* The Customizer API is used to allow simple, real-time editing of many aspects of the site and design.
* Shortcodes implemented to output some of the data entered by clients through the Customizer.
* Javascript and PHP used to bridge gaps in core, plugin, and intended functionality.
* Utilizes a set of premium plugins that we have come to trust at EV to provide additional functionality.
