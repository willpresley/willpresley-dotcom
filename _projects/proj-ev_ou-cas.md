---
layout:       project
published:    true
date:         20150308
short:        "A responsive, CommonSpot-powered site for Ohio University's largest college."
title:        "Ohio University - College of Arts & Sciences"
website:      https://www.ohio.edu/cas/
image-small:
  url:        "https://willpresley.com/uploads/projects/ev_ou-cas/ev-ou-cas-small_1.jpg"
  title:      "Ohio University - College of Arts & Sciences"
  alt:        "Ohio University - College of Arts & Sciences"
image-large:
  url:        "https://willpresley.com/uploads/projects/ev_ou-cas/ev-ou-cas-large_1.jpg"
  title:      "Ohio University - College of Arts & Sciences"
  alt:        "Ohio University - College of Arts & Sciences"
  bordered:   true
---
_Project for Electronic Vision._

A rebuild of the entire website for the College of Arts & Sciences at Ohio University, including its many departments, in the CommonSpot CMS. CommonSpot is a Cold-Fusion powered CMS that is jointly controlled by both the software vendor and the University IT staff. The site is fully responsive, and also features a management dashboard so that individuals from varying academic units can update various parts of their sites easily.

## What I did on this project

* Created a set of templates in CommonSpot that start with a base, and are abstracted out for specific page types.
* Implement a custom responsive grid system based on the provided design.
* Write various helper scripts using both vanilla Javascript and jQuery to address missing/overly-complex aspects of the CMS.
* Create and document a full management dashboard for non-technical users.
* Organize and conduct training sessions for University staff who will be updating various parts of the site.
