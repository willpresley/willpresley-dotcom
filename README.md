# www.willpresley.com

**Built using:**

* Jekyll
* SASS
* Disqus comment system

**Features:**

* Responsive
* Static (fast)
* Simple to update and deploy

Copyright (c) 2015 Will Presley
