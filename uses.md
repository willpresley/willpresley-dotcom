---
layout: "page"
title: "What Do I Use?"
permalink: "/uses/"
page-id: uses
meta-description: "A page with the hardware and software that I use, as seen on uses.tech"
meta-robots: "noodp, noydir"
comments: true
date: 2020-10-01
---

### Inspiration

After seeing [a post by Jan-Lukas Else](https://jlelse.blog/micro/2020/01/things-i-use/) about them, I decided to join the [growing number](https://uses.tech) of folks who are sharing lists of _what they use_. Use the links below to jump to a specific section.

{:.text-center}
**[Hardware](#hardware) \| [Software](#software) \| [Websites](#websites)**

### Hardware

<div markdown="1" class="uses-list-half">

#### Desktop 1 (Home) - Sonata

* Motherboard: [MSI Z97 PC Mate](https://us.msi.com/Motherboard/Z97-PC-Mate/Overview)
* CPU: Intel Core i5 4590
* RAM: 32gb DDR3 (Crucial Ballistix Sport 2x8gb)
* GPU: [EVGA nVidia Geforce GTX 1050 Ti](https://www.evga.com/articles/01063/evga-geforce-gtx-1050-ti-and-1050/)
* Storage: 2x500gb SSD, 1x1tb HDD
* OS: Windows 10 Pro
* Displays: 2x1080p generics
* Case: [Antec Sonata III](https://www.bhphotovideo.com/c/product/809801-REG/Antec_SONATA_III_500_Sonata_III_500.html)

</div>

<div markdown="1" class="uses-list-half">

#### Desktop 2 (Work) - Amory

* Motherboard: [ASRock Fatal1ty Z97 Killer](https://www.asrock.com/mb/Intel/Fatal1ty%20Z97%20Killer/)
* CPU: Intel Core i5 4460
* RAM: 32gb DDR3 (Kingston HyperX FURY 4x8gb)
* GPU: [ASUS nVidia Geforce GT 640](https://www.asus.com/Graphics-Cards/GT6402GD3/)
* Storage: 2x500gb SSD, 1x256gb m.2, 1x1tb HDD
* OS: Windows 10 Pro
* Displays: 2x1080p generics + 1x1080p in portrait
* Case: [Lian Li PC-7 A Plus](https://www.newegg.com/p/N82E16811112211)

</div>

<div markdown="1" class="uses-list-half">

#### Laptop 1 - Newo

* Brand: [Microsoft Surface Book 1](https://en.wikipedia.org/wiki/Surface_Book) - [TechRadar](https://www.techradar.com/reviews/pc-mac/laptops-portable-pcs/laptops-and-netbooks/microsoft-surface-book-1306306/review)
* CPU: Intel Core i5 6300U
* RAM: 8gb
* Storage: 128gb internal, 128gb SD as VHD
* Display Resolution: 3000x2000 pixels
* OS: Windows 10 Pro

</div>

<div markdown="1" class="uses-list-half">

#### Laptop 2

* Brand: [Lenovo ThinkPad T410](https://support.lenovo.com/us/en/solutions/pd006109) - [NotebookReview](notebookreview.com/notebookreview/lenovo-thinkpad-t410-review/)
* CPU: Intel Core i7 620
* RAM: 8gb
* Storage: 128gb SSD
* Display Resolution: 1440x900 pixels
* OS: [Lubuntu](https://lubuntu.net/) 19.04

</div>

<div markdown="1" class="uses-list-half">

#### Home Media Server - Ambellina

* Motherboard: [ASUS P8Z77-V LE](https://www.asus.com/Motherboards/P8Z77V_LE/)
* CPU: Intel Core i5 2400
* RAM: 8gb DDR3 (Samsung 2x4gb)
* GPU: [EVGA nVidia GeForce GTX 750 Ti](https://www.evga.com/products/specs/gpu.aspx?pn=70b14ba6-5853-4a65-aacf-cff61f466d82)
* Storage: 1x250gb SSD, 34.7tb of HDDs in two-way mirror Storage Space (similar-ish to RAID-10) with 1x250gb SSD as write-cache
* OS: Windows 10 Pro
* Case: Some generic Rosewill thing

</div>

<div markdown="1" class="uses-list-half">

#### Phone - [Google Pixel 3](https://store.google.com/product/pixel_3) (128gb) - Verizon

* Root Status: *Not*-rooted
* Launcher: [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher) [Prime](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher.prime)
* Ad-Blocker: [Blokada](https://blokada.org/index.html)
* Weather: [Today Weather](https://play.google.com/store/apps/details?id=mobi.lockdown.weather)
* Keyboard: [Gboard](https://play.google.com/store/apps/details?id=com.google.android.inputmethod.latin)
* Messages/SMS: [Android Messages](https://play.google.com/store/apps/details?id=com.google.android.apps.messaging) (with [Android Messages Desktop](https://github.com/chrisknepper/android-messages-desktop))
* Photos: [Google Photos](https://play.google.com/store/apps/details?id=com.google.android.apps.photos), [Piktures](https://play.google.com/store/apps/details?id=com.diune.pictures)
* Password Manager: [Bitwarden](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden)
* Other Vital Apps: [F-Droid](https://f-droid.org/), [Greenify](https://play.google.com/store/apps/details?id=com.oasisfeng.greenify), [Amazfit Tools](https://play.google.com/store/apps/details?id=cz.zdenekhorak.amazfittools), [andOTP](https://f-droid.org/packages/org.shadowice.flocke.andotp/), [Auto Dark Theme](https://play.google.com/store/apps/details?id=com.cannic.apps.automaticdarktheme), [Backdrops](https://play.google.com/store/apps/details?id=com.backdrops.wallpapers), [CoSy for Facebook](https://play.google.com/store/apps/details?id=com.michaelflisar.cosy.facebook.play), [Pushbullet](https://play.google.com/store/apps/details?id=com.pushbullet.android), [ScreenCam](https://f-droid.org/packages/com.orpheusdroid.screenrecorder/), [Solid Explorer](https://play.google.com/store/apps/details?id=pl.solidexplorer2), [Simple Scrobbler](https://f-droid.org/packages/com.adam.aslfms/), [Tasker](https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm), [Twitch 2! Contacts Formatter](https://play.google.com/store/apps/details?id=twitch.angelandroidapps.cnf2), [Wallpaper Changer](https://play.google.com/store/apps/details?id=de.j4velin.wallpaperChanger), [Wifi Analyzer](https://f-droid.org/packages/com.vrem.wifianalyzer/)

</div>

### Software

<div markdown="1" class="uses-list-half">

#### Productivity & Work

* Email: [Wavebox Pro](https://wavebox.io/)
* Chat: [Slack](https://slack.com/)
* Creative: [Adobe Creative Cloud](https://www.adobe.com/creativecloud.html) + [Inkscape](https://inkscape.org/) + [GIMP](https://www.gimp.org/)
* Office: [LibreOffice Fresh](https://www.libreoffice.org/)
* File Manager: [Total Commander](https://www.ghisler.com/)
* Remote Desktop: [AnyDesk](https://anydesk.com/)
* PDF Reader: [SumatraPDF](https://www.sumatrapdfreader.org/)
* Lists & Deadlines: [Trello](https://trello.com/)
* Screen Capture: [screen2gif](https://www.screentogif.com/) + [ffmpeg](https://www.ffmpeg.org/)

</div>

<div markdown="1" class="uses-list-half">

#### Other Windows Software I Use

* [PlexAmp](https://plexamp.com/)
* [YouTube Music Desktop App](https://github.com/ytmdesktop/ytmdesktop)
* [Tweeten](https://tweetenapp.com/)
* [Calibre](https://calibre-ebook.com/)
* [ComicRack](http://comicrack.cyolito.com/) + [ComicVine Tagger](https://github.com/cbanack/comic-vine-scraper) (or [ComicTagger](https://github.com/comictagger/comictagger))
* [mp3tag](https://www.mp3tag.de/en/)
* [Filebot](https://www.filebot.net/)
* [notepad2-mod](https://xhmikosr.github.io/notepad2-mod/)
* [dnGrep](https://dngrep.github.io/)
* [7-Zip](https://www.7-zip.org/)

</div>

<div markdown="1" class="uses-list-half">

#### Other Development Tools - Windows

* Terminal: [Windows Terminal](https://github.com/microsoft/terminal)
    * Font: [Fira Code](https://github.com/tonsky/FiraCode)
    * Shells: [WSL2](https://docs.microsoft.com/en-us/windows/wsl/wsl2-install) ([Ubuntu](https://ubuntu.com/), [Bash](https://www.gnu.org/software/bash/)), [Powershell Core](https://github.com/PowerShell/PowerShell), CMD
* Git GUI: [Git-Fork](https://git-fork.com/)
* Package Manager: [Chocolatey](https://chocolatey.org/)
* Image Compression: [FileOptimizer](https://nikkhokkho.sourceforge.io/static.php?page=FileOptimizer)
* SFTP: [WinSCP](https://winscp.net/eng/index.php)
* Key Management: [PUTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/)
* Virtualization: [VMWare Workstation](https://www.vmware.com/products/workstation-pro.html)
* Remote Version Control: [BitBucket](https://bitbucket.org/product)

</div>

<div markdown="1" class="uses-list-half">

#### Other Development Tools - Linux

* *Someday I'll get this together, but mostly iTerm2, nano, Firefox*

</div>

<div markdown="1" class="uses-list-half">

#### Code Editor: VS Code Insiders

* **Extensions**: [Settings Sync](https://marketplace.visualstudio.com/items?itemName=Shan.code-settings-sync), [Alignment](https://marketplace.visualstudio.com/items?itemName=annsk.alignment), [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag), [Auto Time Stamp](https://marketplace.visualstudio.com/items?itemName=lpubsppop01.vscode-auto-timestamp), [Autoprefixer](https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-autoprefixer), [Beautify](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify), [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2), [Can I Use](https://marketplace.visualstudio.com/items?itemName=akamud.vscode-caniuse), [Color Highlight](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight), [CSS Peek](https://marketplace.visualstudio.com/items?itemName=pranaygp.vscode-css-peek), [JS & CSS Minifier](https://marketplace.visualstudio.com/items?itemName=olback.es6-css-minify), [Live Sass Compiler](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass), [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer), [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint), [PHP Intelephense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client), [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode), [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python), [Remote - WSL](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl), [SCSS Everywhere](https://marketplace.visualstudio.com/items?itemName=gencer.html-slim-scss-css-class-completion), [SCSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-scss), [Spell Right](https://marketplace.visualstudio.com/items?itemName=ban.spellright), [SVG](https://marketplace.visualstudio.com/items?itemName=jock.svg), [SVG Viewer](https://marketplace.visualstudio.com/items?itemName=cssho.vscode-svgviewer), [Trailing Spaces](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces), [WooCommerce - Snippets](https://marketplace.visualstudio.com/items?itemName=claudiosanches.woocommerce), [Wordpress Snippets](https://marketplace.visualstudio.com/items?itemName=wordpresstoolbox.wordpress-toolbox), [XML Tools](https://marketplace.visualstudio.com/items?itemName=DotJoshJohnson.xml)
* Font: [Fira Code](https://github.com/tonsky/FiraCode)
* Color Theme: [Monokai Dark Soda](https://marketplace.visualstudio.com/items?itemName=AdamCaviness.theme-monokai-dark-soda) or [Material Theme](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-material-theme)
* File Icon Theme: [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)

</div>

<div markdown="1" class="uses-list-half">

#### Browser: Google Chrome

* **Extensions**: [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) + [Extra](https://chrome.google.com/webstore/detail/ublock-origin-extra/pgdnlhfefecpicbbihgmbmffkjpaplco) + [Nano Defender](https://chrome.google.com/webstore/detail/nano-defender/ggolfgbegefeeoocgjbmkembbncoadlb), [Auto Tab Discard](https://chrome.google.com/webstore/detail/auto-tab-discard/jhnleheckmknfcgijgkadoemagpecfol/), [AutoplayStopper](https://chrome.google.com/webstore/detail/autoplaystopper/ejddcgojdblidajhngkogefpkknnebdh), [Bitwarden](https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb), [ColorZilla](https://chrome.google.com/webstore/detail/colorzilla/bhlhnicpbhignbdhedgjhgdocnmhomnp), [Dark Reader](https://chrome.google.com/webstore/detail/dark-reader/eimadpbcbfnmbkopoojfekhnkhdbieeh), [DevTools Author](https://chrome.google.com/webstore/detail/devtools-author/egfhcfdfnajldliefpdoaojgahefjhhi), [Dimensions](https://chrome.google.com/webstore/detail/dimensions/baocaagndhipibgklemoalmkljaimfdj), [FluffBusting Purity](https://chrome.google.com/webstore/detail/fbfluffbustingpurity/nmkinhboiljjkhaknpaeaicmdjhagpep), [Just Read](https://chrome.google.com/webstore/detail/just-read/dgmanlpmmkibanfdgjocnabmcaclkmod), [LanguageTool](https://chrome.google.com/webstore/detail/grammar-and-spell-checker/oldceeleldhonbafppcapldpdifcinji) [Nimbus Screenshot](https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj), [Octo Mate](https://chrome.google.com/webstore/detail/octo-mate/baggcehellihkglakjnmnhpnjmkbmpkf), [Old Reddit Redirect](https://chrome.google.com/webstore/detail/old-reddit-redirect/dneaehbmnbhcippjikoajpoabadpodje), [PopUpOFF](https://chrome.google.com/webstore/detail/popupoff-popup-and-overla/ifnkdbpmgkdbfklnbfidaackdenlmhgh), [Pushbullet](https://chrome.google.com/webstore/detail/pushbullet/chlffgpmiacpedhhbkiomidkjlcfhogd), [Reddit Enhancement Suite](https://chrome.google.com/webstore/detail/reddit-enhancement-suite/kbmfpngjjgdllneeigpgjifpgocmfgmb), [Save to Pocket](https://chrome.google.com/webstore/detail/save-to-pocket/niloccemoadcdkdjlinkgdfekeahmflj), [Smile Always](https://chrome.google.com/webstore/detail/smile-always/jgpmhnmjbhgkhpbgelalfpplebgfjmbf), [Tab Session Manager](https://chrome.google.com/webstore/detail/tab-session-manager/iaiomicjabeggjcfkbimgmglanimpnae/), [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo), [Toolbox for Google Play Store](https://chrome.google.com/webstore/detail/toolbox-for-google-play-s/fepaalfjfchbdianlgginbmpeeacahoo), [Toucan - Spanish](https://chrome.google.com/webstore/detail/toucan/lokjgaehpcnlmkebpmjiofccpklbmoci), [Tracking Token Stripper](https://chrome.google.com/webstore/detail/tracking-token-stripper/kcpnkledgcbobhkgimpbmejgockkplob), [View Image Info](https://chrome.google.com/webstore/detail/view-image-info-propertie/jldjjifbpipdmligefcogandjojpdagn)

</div>
